<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/customer','PagesController@customer');
Route::get('/dashboard','PagesController@dashboard');
Route::get('/paket laundry','PagesController@paketlaundry');
Route::get('/status pembayaran','PagesController@statuspembayaran');
Route::get('/status pesanan','PagesController@statuspesanan');
Route::get('/transaksi pesanan','PagesController@transaksipesanan');
