<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function customer()
    {
        return view('customer');
    }
    public function dashboard()
    {
        return view('dashboard');
    }
    public function paketlaundry()
    {
        return view('paket laundry');
    }
    public function statuspembayaran()
    {
        return view('status pembayaran');
    }
    public function statuspesanan()
    {
        return view('status pesanan');
    }
    public function transaksipesanan()
    {
        return view('transaksi pesanan');
    }
}
